const express = require("express");
const mongoose = require ("mongoose");

const taskRoute = require("./Routes/taskRoute");
// const userRoute = require("./Routes/taskRoute");

const app = express();
const port = 3001;

// mongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch245-dagon.wirp2aj.mongodb.net/s35-discussion?retryWrites=true&w=majority",

{	
	//allow us to avoid any current  and future  error  while  connecting  to mongoDB
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

//error catcher

db.on("error", console.error.bind(console,"Connection Error!"));
db.once("open",() => console.log("We are now connected to the cloud"));

// Middlewares

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// routing

app.use("/tasks", taskRoute);
// app.use("/signup",taskRoute);



app.listen(port,()=> console.log(`Server is running at port ${port}`));


// Separation of concern:
    // model should be connected to the controller.
        // controller should be connected to the Routes.
        // Route should be connected to the server/application
