const express = require("express");
const router = express.Router();

const userController = require("../Controllers/userControllers");

router.post("/", userController.newUser);


module.exports=router;