const express = require("express");
const router = express.Router();


const taskController = require("../Controllers/taskControllers");
// const userController = require("../Controllers/userController");


// Routes for getAll

router.get("/", taskController.getAll);


// Router for post

router.post("/addTask",taskController.createTask);


router.delete("/deleteTask/:id",taskController.deleteTask);

router.get("/:id", taskController.getId)

router.put("/:id/complete", taskController.updateStatus);















module.exports=router;


