const { response, request } = require("express");
const Task = require("../Models/task");

// Controller and functions 

//Controller/function to get all the task on our data base

module.exports.getAll = (request, response)=>{

    Task.find({}).then(result => {
        return response.send(result);
    })
    .catch(error=> {
        return response.send(error);
    })

}

module.exports.createTask = (request, response)=>{

    const input= request.body;

    Task.findOne({name: input.name})
    .then(result =>{
        if(result !== null){
            return response.send("The task is already existing");
        }else{
            let newTask = new Task({
                name: input.name
            });

            newTask.save().then(save => {
                return response.send("The task is successfully added!")
            }).catch(error => {
                return response.send(error)
            })
        }


      })
    .catch(error =>{
        return response.send(error)
    });

};

//Controller that will delete the document that contains the given object

module.exports.deleteTask = (request, response) =>{
    let idToBeDeleted = request.params.id;

    //findByIdAndRemove  - to find the document that contains the id and then delete the document

    Task.findByIdAndRemove(idToBeDeleted)
    .then(result =>{
        return response.send(result)
    })
    .catch(error =>{
        return response.send(error)
    })
}

module.exports.getId = (request, response) => {

    let idToBeUpdated = request.params.id;

    Task.findByIdAndUpdate(idToBeUpdated)
    .then(result =>{
        return response.send(result)
    })
    .catch(error =>{
        return response.send(error)
    })

}

module.exports.updateStatus= (request, response)=>{

    let idToBeUpdated = request.params.id;

    Task.findByIdAndUpdate(idToBeUpdated,{status:"complete"},{new:true})
    .then(result =>{
     
            return response.send(result)

         })
      
      
    .catch(error => {
                return response.send(error)
            })
        
}
   
    

